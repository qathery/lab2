﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRES = new System.Windows.Forms.Button();
            this.A = new System.Windows.Forms.TextBox();
            this.B = new System.Windows.Forms.TextBox();
            this.C = new System.Windows.Forms.TextBox();
            this.D = new System.Windows.Forms.TextBox();
            this.X1 = new System.Windows.Forms.TextBox();
            this.X2 = new System.Windows.Forms.TextBox();
            this.labelA = new System.Windows.Forms.Label();
            this.labelB = new System.Windows.Forms.Label();
            this.labelC = new System.Windows.Forms.Label();
            this.labelD = new System.Windows.Forms.Label();
            this.labelX1 = new System.Windows.Forms.Label();
            this.labelX2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonRES
            // 
            this.buttonRES.Location = new System.Drawing.Point(269, 147);
            this.buttonRES.Name = "buttonRES";
            this.buttonRES.Size = new System.Drawing.Size(203, 111);
            this.buttonRES.TabIndex = 0;
            this.buttonRES.Text = "res";
            this.buttonRES.UseVisualStyleBackColor = true;
            this.buttonRES.Click += new System.EventHandler(this.Button1_Click);
            // 
            // A
            // 
            this.A.Location = new System.Drawing.Point(269, 25);
            this.A.Name = "A";
            this.A.Size = new System.Drawing.Size(203, 22);
            this.A.TabIndex = 1;
            // 
            // B
            // 
            this.B.Location = new System.Drawing.Point(269, 70);
            this.B.Name = "B";
            this.B.Size = new System.Drawing.Size(203, 22);
            this.B.TabIndex = 2;
            // 
            // C
            // 
            this.C.Location = new System.Drawing.Point(269, 119);
            this.C.Name = "C";
            this.C.Size = new System.Drawing.Size(203, 22);
            this.C.TabIndex = 3;
            // 
            // D
            // 
            this.D.Location = new System.Drawing.Point(269, 264);
            this.D.Name = "D";
            this.D.Size = new System.Drawing.Size(203, 22);
            this.D.TabIndex = 4;
            // 
            // X1
            // 
            this.X1.Location = new System.Drawing.Point(269, 306);
            this.X1.Name = "X1";
            this.X1.Size = new System.Drawing.Size(203, 22);
            this.X1.TabIndex = 5;
            // 
            // X2
            // 
            this.X2.Location = new System.Drawing.Point(269, 344);
            this.X2.Name = "X2";
            this.X2.Size = new System.Drawing.Size(203, 22);
            this.X2.TabIndex = 6;
            // 
            // labelA
            // 
            this.labelA.AutoSize = true;
            this.labelA.Location = new System.Drawing.Point(249, 30);
            this.labelA.Name = "labelA";
            this.labelA.Size = new System.Drawing.Size(17, 17);
            this.labelA.TabIndex = 7;
            this.labelA.Text = "A";
            // 
            // labelB
            // 
            this.labelB.AutoSize = true;
            this.labelB.Location = new System.Drawing.Point(249, 73);
            this.labelB.Name = "labelB";
            this.labelB.Size = new System.Drawing.Size(17, 17);
            this.labelB.TabIndex = 8;
            this.labelB.Text = "B";
            // 
            // labelC
            // 
            this.labelC.AutoSize = true;
            this.labelC.Location = new System.Drawing.Point(249, 122);
            this.labelC.Name = "labelC";
            this.labelC.Size = new System.Drawing.Size(17, 17);
            this.labelC.TabIndex = 9;
            this.labelC.Text = "C";
            // 
            // labelD
            // 
            this.labelD.AutoSize = true;
            this.labelD.Location = new System.Drawing.Point(245, 269);
            this.labelD.Name = "labelD";
            this.labelD.Size = new System.Drawing.Size(18, 17);
            this.labelD.TabIndex = 10;
            this.labelD.Text = "D";
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            this.labelX1.Location = new System.Drawing.Point(238, 309);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(25, 17);
            this.labelX1.TabIndex = 11;
            this.labelX1.Text = "X1";
            // 
            // labelX2
            // 
            this.labelX2.AutoSize = true;
            this.labelX2.Location = new System.Drawing.Point(238, 349);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(25, 17);
            this.labelX2.TabIndex = 12;
            this.labelX2.Text = "X2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.labelD);
            this.Controls.Add(this.labelC);
            this.Controls.Add(this.labelB);
            this.Controls.Add(this.labelA);
            this.Controls.Add(this.X2);
            this.Controls.Add(this.X1);
            this.Controls.Add(this.D);
            this.Controls.Add(this.C);
            this.Controls.Add(this.B);
            this.Controls.Add(this.A);
            this.Controls.Add(this.buttonRES);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonRES;
        private System.Windows.Forms.TextBox A;
        private System.Windows.Forms.TextBox B;
        private System.Windows.Forms.TextBox C;
        private System.Windows.Forms.TextBox D;
        private System.Windows.Forms.TextBox X1;
        private System.Windows.Forms.TextBox X2;
        private System.Windows.Forms.Label labelA;
        private System.Windows.Forms.Label labelB;
        private System.Windows.Forms.Label labelC;
        private System.Windows.Forms.Label labelD;
        private System.Windows.Forms.Label labelX1;
        private System.Windows.Forms.Label labelX2;
    }
}

