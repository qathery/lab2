﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Class1
    {
         public static double D(double a, double b, double c, out double d )
        {
            d = b * b - 4 * a * c;
            return d;
        }
        public static double X1(double a, double b,  double d, out double x1)
        {
            x1 = -b - Math.Sqrt(d) / (2 * a);
            return x1;
        }
        public static double X2(double a, double b, double d, out double x2)
        {
            x2 = -b + Math.Sqrt(d) / (2 * a);
            return x2;
        }

    }
}
